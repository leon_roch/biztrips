import { Header } from "./components/Header";
import logo from "./assets/logo.png";
import { Catalog } from "./components/Catalog";
import { Trip } from "./types/Trip";
import { useEffect, useState } from "react";
import { tripsDB } from "./data/db";
import { VStack } from "@chakra-ui/react";
import { Wishlist } from "./components/Wishlist";
import { Footer } from "./components/Footer";
// TODO: Testing docs https://dev.to/hannahadora/jest-testing-with-vite-and-react-typescript-4bap

function App() {
  const [trips, setTrips] = useState<Trip[]>([]);
  const [wishlistedTrips, setWishlistedTrips] = useState<Trip[]>([]);

  useEffect(() => {
    setTrips(tripsDB.find());
  }, []);

  function handleCatalogTripClick(tripId: number) {
    if (wishlistedTrips.find((t) => t.id === tripId)) return;
    const trip = trips.find((t) => t.id === tripId);
    setWishlistedTrips((old) => [...old, trip!]);
  }

  function handleWishlistTripRemove(tripId: number) {
    setWishlistedTrips((old) => old.filter((t) => t.id !== tripId));
  }

  return (
    <>
      <Header logo={logo} />
      <VStack width={"100%"} gap={10} paddingX={{ base: 3, md: 10 }}>
        <Catalog trips={trips} onTripClick={handleCatalogTripClick} />
        <Wishlist
          trips={wishlistedTrips}
          onRemoveTrip={handleWishlistTripRemove}
        />
      </VStack>
      <Footer />
    </>
  );
}

export default App;
