import {
  Heading,
  IconButton,
  Table,
  TableContainer,
  Tbody,
  Td,
  Text,
  Tfoot,
  Th,
  Thead,
  Tooltip,
  Tr,
  VStack,
} from "@chakra-ui/react";
import { Trip } from "../types/Trip";
import { DeleteIcon } from "@chakra-ui/icons";

interface IWishlistProps {
  trips: Trip[];
  onRemoveTrip?: (tripid: number) => void;
}

export function Wishlist({ trips, onRemoveTrip = () => {} }: IWishlistProps) {
  return (
    <VStack width={"100%"}>
      <Heading>Wishlist</Heading>
      <TableContainer
        width={"100%"}
        rounded={"md"}
        borderColor={"gray.300"}
        borderWidth={1}
        borderStyle={"solid"}
        boxShadow={"md"}
      >
        <Table size={{ base: "sm", lg: "lg" }}>
          <Thead>
            <Tr>
              <Th>Trip</Th>
              <Th>Price</Th>
              <Th>Actions</Th>
            </Tr>
          </Thead>
          <Tbody>
            {trips.length === 0 ? (
              <Tr>
                <Td>
                  <Text>No trips added yet</Text>
                </Td>
                <Td></Td>
                <Td></Td>
              </Tr>
            ) : (
              <>
                {trips.map((trip) => (
                  <Tr key={trip.id}>
                    <Td>
                      <Text>{trip.title}</Text>
                    </Td>
                    <Td>
                      <Text>{trip.price}</Text>
                    </Td>
                    <Td>
                      <IconButton
                        aria-label="Remove trip"
                        icon={<DeleteIcon />}
                        onClick={() => onRemoveTrip(trip.id)}
                      />
                    </Td>
                  </Tr>
                ))}
              </>
            )}
          </Tbody>
          <Tfoot>
            <Tr>
              <Th>Total</Th>
              <Th>{trips.reduce((acc, curr) => acc + curr.price, 0)}</Th>
              <Th>
                <Tooltip label="Remove all">
                  <IconButton
                    aria-label="Remove trip"
                    icon={<DeleteIcon />}
                    onClick={() =>
                      trips.forEach((trip) => onRemoveTrip(trip.id))
                    }
                  />
                </Tooltip>
              </Th>
            </Tr>
          </Tfoot>
        </Table>
      </TableContainer>
    </VStack>
  );
}
