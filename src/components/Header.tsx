import { Flex, Heading, Image } from "@chakra-ui/react";

interface IHeaderProps {
  title?: string;
  logo?: string;
}

export function Header({ title, logo }: IHeaderProps) {
  return (
    <Flex
      width={"100%"}
      paddingX={5}
      paddingTop={2}
      direction={"row"}
      justify={"space-between"}
    >
      <Image height={"50px"} src={logo} alt="logo" />
      <Heading>{title}</Heading>
    </Flex>
  );
}
