import { CalendarIcon, TimeIcon } from "@chakra-ui/icons";
import {
  Button,
  VStack,
  HStack,
  Tooltip,
  Tag,
  Image,
  Text,
} from "@chakra-ui/react";
import { useMemo } from "react";
import { Trip } from "../types/Trip";

interface ICatalogItemProps {
  trip: Trip;
  onTripClick?: (tripId: number) => void;
}

export function CatalogItem({
  trip,
  onTripClick = () => {},
}: ICatalogItemProps) {
  const price = useMemo(
    () =>
      trip.price.toLocaleString(undefined, {
        style: "currency",
        currency: "USD",
        maximumFractionDigits: 0,
      }),
    [trip.price]
  );

  const duration = useMemo(
    () =>
      Math.ceil(
        (trip.endDate.getTime() - trip.startDate.getTime()) /
          (1000 * 60 * 60 * 24)
      ),
    [trip.startDate, trip.endDate]
  );

  const startDate = useMemo(
    () =>
      trip.startDate.toLocaleDateString(undefined, {
        month: "short",
        day: "numeric",
        year: "numeric",
      }),
    [trip.startDate]
  );

  return (
    <Button
      onClick={() => onTripClick(trip.id)}
      height={"auto"}
      variant={"outline"}
      paddingY={3}
    >
      <VStack gap={2} width={"100%"} overflow={"hidden"}>
        <Image
          src={trip.image}
          alt={trip.title}
          aspectRatio={"1 / 1"}
          rounded={"md"}
        />
        <Text width={"100%"} textAlign={"center"}>
          {trip.title}
        </Text>
        <HStack justify={"center"} gap={3} width={"100%"} overflowX={"auto"}>
          <Tooltip label={"start date"}>
            <Tag>
              <CalendarIcon marginRight={2} /> {startDate}
            </Tag>
          </Tooltip>
          <Tooltip label={"duration"}>
            <Tag>
              <TimeIcon marginRight={2} /> {duration} days
            </Tag>
          </Tooltip>
          <Tooltip label={"price"}>
            <Tag>{price}</Tag>
          </Tooltip>
        </HStack>
        <Text
          textAlign={"center"}
          width={"100%"}
          overflow={"hidden"}
          whiteSpace={"normal"}
        >
          {trip.description}
        </Text>
      </VStack>
    </Button>
  );
}
