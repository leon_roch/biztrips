import { Heading, Select, SimpleGrid, VStack } from "@chakra-ui/react";
import { Trip } from "../types/Trip";
import { useEffect, useState } from "react";
import { months, tripDuringMonth } from "../utils";
import { CatalogItem } from "./CatalogItem";

interface ICatalogProps {
  trips: Trip[];
  onTripClick?: (tripId: number) => void;
}

export function Catalog({ trips, onTripClick = () => {} }: ICatalogProps) {
  const [selectedMonth, setSelectedMonth] = useState<number>(0);

  const [filteredTrips, setFilteredTrips] = useState<Trip[]>(trips);

  useEffect(() => {
    if (selectedMonth === 0 || trips.length === 0) {
      setFilteredTrips(trips);
    } else {
      setFilteredTrips(trips.filter(tripDuringMonth(selectedMonth)));
    }
  }, [selectedMonth, trips]);

  return (
    <VStack justify={"start"} width={"100%"}>
      <Heading>Catalog</Heading>
      <Select
        value={selectedMonth}
        onChange={(e) => setSelectedMonth(Number(e.target.value))}
      >
        {Object.entries(months).map(([display, value]) => (
          <option key={value} value={value}>
            {display}
          </option>
        ))}
      </Select>
      {filteredTrips.length === 0 ? (
        <Heading marginTop={"20px"} fontSize={"large"}>
          No Trips Found
        </Heading>
      ) : (
        <SimpleGrid
          columns={{ base: 1, md: 2, lg: 3 }}
          spacingX={10}
          spacingY={5}
          width={"100%"}
        >
          {filteredTrips.map((trip) => (
            <CatalogItem trip={trip} onTripClick={onTripClick} key={trip.id} />
          ))}
        </SimpleGrid>
      )}
    </VStack>
  );
}
