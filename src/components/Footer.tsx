import { Heading, chakra } from "@chakra-ui/react";

export function Footer() {
  return (
    <chakra.div width={"100%"} paddingTop={50} paddingBottom={"20px"}>
      <Heading color={"gray.600"} textAlign={"center"} fontSize={"medium"}>
        © {new Date().getFullYear()} Léonard Roch. All rights reserved.
      </Heading>
    </chakra.div>
  );
}
