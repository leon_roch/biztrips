export class DataStore<T> {
  private _store: T[];
  private _id: keyof T;

  constructor(id: keyof T, initialData: T[] = []) {
    this._id = id;
    this._store = initialData;
  }

  private _equals<Type>(obj: Type, toCheck: any): boolean {
    if (Array.isArray(obj) && JSON.stringify(obj) !== JSON.stringify(toCheck)) {
      return false; // array comparison
    }
    if (typeof obj !== "object" && obj !== toCheck) return false; // non object comparison
    for (const [key, value] of Object.entries(toCheck)) {
      const attrToCompare: Type[keyof Type] = obj[key as keyof Type];
      if (!this._equals(attrToCompare, value)) return false; // object comparison
    }
    return true;
  }

  find(criteria?: Partial<T>): T[] {
    if (!criteria) return this._store;
    return this._store.filter((item) => {
      return this._equals(item, criteria);
    });
  }

  findById(id: T[keyof T]): T | undefined {
    return this._store.find((item) =>
      this._equals(item, { [this._id as string]: id })
    );
  }

  findOne(criteria: Partial<T>): T | undefined {
    return this._store.find((item) => this._equals(item, criteria));
  }

  private _findIndex(criteria: Partial<T>): number {
    return this._store.findIndex((item) => this._equals(item, criteria));
  }

  findOneOr(criteria: Partial<T>, or: () => void = () => {}): T | void {
    const found = this._store.find((item) => this._equals(item, criteria));
    if (!found) return or();
    return found;
  }

  create(newOne: T): T {
    this._store.push(newOne);
    return newOne;
  }

  delete(criteria: Partial<T>): T | undefined {
    const index = this._findIndex(criteria);
    if (index === -1) return undefined;
    return this._store.splice(index, 1)[0];
  }

  patch(criteria: Partial<T>, replacement: Partial<T>): T | undefined {
    const index = this._findIndex(criteria);
    if (index === -1) return undefined;
    const updated = {
      ...this._store[index],
      ...replacement,
    };
    this._store.splice(index, 1, updated);
    return updated;
  }

  replace(criteria: Partial<T>, replacement: T): T | undefined {
    return this.patch(criteria, replacement);
  }
}
