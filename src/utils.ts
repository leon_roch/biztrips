import { Trip } from "./types/Trip";

export const months = {
  "All Months": 0,
  January: 1,
  February: 2,
  March: 3,
  April: 4,
  May: 5,
  June: 6,
  July: 7,
  August: 8,
  September: 9,
  October: 10,
  November: 11,
  December: 12,
};

export function tripDuringMonth(month: number): (trip: Trip) => boolean {
  return (trip: Trip) => {
    if (month == 0) return true;
    return trip.startDate.getMonth() + 1 === month;
  };
}
