import { Trip } from "../types/Trip";
import { DataStore } from "../utils/store";

export const tripsDB: DataStore<Trip> = new DataStore("id", [
  {
    id: 1,
    image: "san_francisco.jpg",
    title: "San Francisco World Trade Center",
    description:
      "Enyoy the business trip to San Francisco with nice sightseeing und a lot of fun.",
    price: 1000,
    startDate: new Date(2021, 2, 13, 0, 0),
    endDate: new Date(2021, 2, 15, 16, 56),
    meetings: [
      {
        id: 1,
        title: "One Conference",
        description: "Key Note on One Conference",
      },
      {
        id: 2,
        title: "Zero Conference",
        description: "Workshop Zero on One Conference",
      },
    ],
  },
  {
    id: 2,
    image: "santa_clara.jpg",
    title: "Santa Clara Halley",
    description:
      "Enyoy the business trip to Santa Clara with nice sightseeing und a lot of fun.",
    price: 2000,
    startDate: new Date(2021, 6, 23, 9, 0),
    endDate: new Date(2021, 6, 27, 16, 56),
    meetings: [
      {
        id: 3,
        title: "One Conference",
        description: "HandsOn on One Conference",
      },
      {
        id: 4,
        title: "One Conference",
        description: "Key Note on One Conference",
      },
    ],
  },
  {
    id: 3,
    image: "san_jose.jpg",
    title: "San Jose City Halley",
    description:
      "Enyoy the business trip to San Jose with nice sightseeing und a lot of fun.",
    price: 3000,
    startDate: new Date(2021, 12, 13, 9, 0),
    endDate: new Date(2021, 12, 15, 16, 56),
    meetings: [
      {
        id: 5,
        title: "One Conference",
        description: "Key Note on One Conference",
      },
    ],
  },
]);
