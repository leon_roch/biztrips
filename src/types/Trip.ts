import { Meeting } from "./Meeting";

export interface Trip {
  id: number;
  title: string;
  description: string;
  price: number;
  image: string;
  startDate: Date;
  endDate: Date;
  meetings: Meeting[];
}
