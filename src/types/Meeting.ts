export interface Meeting {
  id: number;
  title: string;
  description: string;
}
