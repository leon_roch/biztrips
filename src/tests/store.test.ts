import { DataStore } from "../utils/store";

const data = [
  {
    id: 1,
    name: "John",
    age: 20,
  },
  {
    id: 2,
    name: "Jane",
    age: 21,
  },
  {
    id: 3,
    name: "Jack",
    age: 22,
  },
];

describe("Datastore", () => {
  let store: DataStore<{ id: number; name: string; age: number }>;

  beforeEach(() => {
    store = new DataStore("id", data);
  });

  it("should be able to get all the data from the store", () => {
    expect(store.find()).toEqual(data);
  });

  it("should be able to get a element by id", () => {
    expect(store.findById(1)).toEqual(data.find((item) => item.id === 1));
  });

  it("should return undefined if no element is found by id", () => {
    expect(store.findById(4)).toBeUndefined();
  });
  it("should return undefined if no element is found by criteria", () => {
    expect(store.findOne({ name: "Sally" })).toBeUndefined();
  });
  it("should delete an element by id", () => {
    store.delete({
      id: 1,
    });
    expect(store.find()).toEqual(data.filter((item) => item.id !== 1));
  });
});
