import { fireEvent, render } from "@testing-library/react";
import { Wishlist } from "../components/Wishlist";
import { Trip } from "../types/Trip";
import { tripsDB } from "../data/db";

const trip: Trip = {
  id: 1,
  image: "san_francisco.jpg",
  title: "San Francisco World Trade Center",
  description:
    "Enyoy the business trip to San Francisco with nice sightseeing und a lot of fun.",
  price: 1000,
  startDate: new Date(2021, 2, 13, 0, 0),
  endDate: new Date(2021, 2, 15, 16, 56),
  meetings: [],
};

describe("Wishlist Component", () => {
  jest.spyOn(tripsDB, "find").mockReturnValue([trip]);

  it("should show that the wishlist is empty", () => {
    const { getByText } = render(<Wishlist trips={[]} />);
    expect(getByText("No trips added yet")).toBeInTheDocument();
  });
  it("should show that a trip is found", () => {
    const { getByText } = render(<Wishlist trips={tripsDB.find()} />);
    expect(getByText(trip.title)).toBeVisible();
  });
  it("should react on remove click", () => {
    const onRemoveTrip = jest.fn();
    const { getAllByRole } = render(
      <Wishlist trips={tripsDB.find()} onRemoveTrip={onRemoveTrip} />
    );
    fireEvent.click(getAllByRole("button")[0]);
    expect(onRemoveTrip).toHaveBeenCalledWith(trip.id);
  });
  it("should react on the remove all click", () => {
    const onRemoveTrip = jest.fn();
    const { getAllByRole } = render(
      <Wishlist trips={tripsDB.find()} onRemoveTrip={onRemoveTrip} />
    );
    fireEvent.click(getAllByRole("button")[1]);
    expect(onRemoveTrip).toHaveBeenCalledWith(trip.id);
  });
});
