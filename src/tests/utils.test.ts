import { Trip } from "../types/Trip";
import { months, tripDuringMonth } from "../utils";

const trips: Trip[] = [
  {
    id: 1,
    title: "Trip to Empire State building",
    startDate: new Date("2019-01-01"),
    endDate: new Date("2019-01-05"),
    description: "Trip to Empire State building",
    image: "img-1.jpg",
    price: 3000,
    meetings: [],
  },
  {
    id: 2,
    title: "Trip to Somewhere",
    startDate: new Date("2019-05-01"),
    endDate: new Date("2019-05-05"),
    description: "Trip to Somewhere",
    image: "img-2.jpg",
    price: 4000,
    meetings: [],
  },
];

describe("test tripDuringMonth", () => {
  it("should return true to all trips", () => {
    expect(trips.every(tripDuringMonth(months["All Months"]))).toBe(true);
  });
  it("should not return true to any trips", () => {
    expect(trips.every(tripDuringMonth(months["February"]))).toBe(false);
  });
});
