import { Catalog } from "../components/Catalog";
import { render, fireEvent } from "@testing-library/react";
import { tripsDB } from "../data/db";

const trips = [
  {
    id: 1,
    image: "san_francisco.jpg",
    title: "San Francisco World Trade Center",
    description:
      "Enyoy the business trip to San Francisco with nice sightseeing und a lot of fun.",
    price: 1000,
    startDate: new Date(2021, 2, 13, 0, 0),
    endDate: new Date(2021, 2, 15, 16, 56),
    meetings: [],
  },
  {
    id: 2,
    image: "santa_clara.jpg",
    title: "Santa Clara Halley",
    description:
      "Enyoy the business trip to Santa Clara with nice sightseeing und a lot of fun.",
    price: 2000,
    startDate: new Date(2021, 6, 23, 9, 0),
    endDate: new Date(2021, 6, 27, 16, 56),
    meetings: [],
  },
];

describe("Catalog Component", () => {
  jest.spyOn(tripsDB, "find").mockReturnValue(trips);

  it("should show that the catalog is empty", () => {
    const { getByText } = render(<Catalog trips={[]} />);
    expect(getByText("No Trips Found")).toBeInTheDocument();
  });

  it("should show that the selection was changed to January", () => {
    const { getByText } = render(<Catalog trips={[]} />);
    fireEvent.click(getByText("All Months"));
    fireEvent.click(getByText("January"));
    expect(getByText("January")).toBeVisible();
  });

  it("should show that a trip is found", () => {
    const { getByText } = render(<Catalog trips={tripsDB.find()} />);
    expect(getByText(trips[0].title)).toBeVisible();
  });
  it("should react on trip click", () => {
    const onTripClick = jest.fn();
    const { getByText } = render(
      <Catalog trips={tripsDB.find()} onTripClick={onTripClick} />
    );
    fireEvent.click(getByText(trips[0].title));
    expect(onTripClick).toHaveBeenCalledWith(trips[0].id);
  });
});
