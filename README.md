# Biztrips

Biztrips is a web application that allows businesses to book business trips for their employees.

[![landing page](docs/landing-page.png)](https://sparkling-pavlova-fe8458.netlify.app/)

_This is a School Project to exercise testing and CI/CD._

## Demo

You can try the application [here](https://sparkling-pavlova-fe8458.netlify.app/).

## Technologies

It's a [node.js](https://nodejs.org/en/) [typescript](https://www.typescriptlang.org/) [react](https://reactjs.org/) application. That uses [chakra-ui](https://chakra-ui.com/) for the UI.

## Testing

The test concept can be found [here](docs/Testkonzept.md). (in German)

## Contact

Feel free to contact me at [leonardroch44+biztrips@gmail.com](mailto:leonardroch44+biztrips@gmail.com) if you have any questions.

## License

The project is licensed under [All Rights Reserved](LICENSE).
