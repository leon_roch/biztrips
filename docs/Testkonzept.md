# Testkonzept von Leonard Roch & Sananjayan Paramanantharajah

## 1. Zielsetzung:

- Ziel ist es, sicherzustellen, dass die `Wishlist` und `Category` Komponenten korrekt gerendert werden und ihre Funktionen ordnungsgemäss erfüllen.
- Zielgruppen sind Unternehmen, die die Plattform nutzen, und deren Mitarbeiter, die Geschäftsreisen buchen.

## 2. Testumgebung:

- Node.js-Version: v20.10.0
- React-Version: 18.2.0
- Testing-Libraries:
  ```json
  "@testing-library/jest-dom": "^6.2.0",
  "@testing-library/react": "^14.1.2",
  "@types/jest": "^29.5.11",
  "jest": "^29.7.0",
  "ts-jest": "^29.1.1",
  "jest-environment-jsdom": "^29.7.0"
  ```

## 3. Testobjekte:

`Wishlist` und `Category` Komponenten, Utility-Funktionen, `store` Klasse.

# 4. Testarten:

- Komponententests für `Wishlist` und `Category` mit `@testing-library/react` und `@testing-library/jest-dom`.
- Unit-Tests für Utility-Funktionen mit jest.
- Unit-Tests für die `store` Klasse mit jest.

## 5. Testkriterien und -merkmale:

Funktionalität, korrekte Darstellung, Benutzerinteraktionen.

## 6. Teststrategie:

Kombination aus automatisierten Komponententests und Unit-Tests.
Schwerpunkt auf Mocking und Isolierung von Komponenten und Funktionen.

## 7. Testphasen und -abläufe:

Die Tests werden in beliebiger Reihenfolge ausgeführt, da sie unabhängig voneinander sind:

- Komponententests für `Wishlist` und `Category`.
- Unit-Tests für Utility-Funktionen.
- Unit-Tests für die `store` Klasse.

## 8. Risikobewertung:

Risiko von Anzeigefehlern oder Benutzerinteraktionsproblemen in den Komponenten.
Risiko von Fehlern in den Utility-Funktionen oder der Store-Klasse, die die Datenverwaltung beeinträchtigen könnten.

## 9. Testwerkzeuge und -infrastruktur:

- `@testing-library/react` für Komponententests.
- `jest` als Test-Framework für Unit-Tests, mit der Verwendung von `jest --coverage` zur Messung der Testabdeckung.
- Gitlab CI/CD für die automatisierte Ausführung von Tests. Die Tests werden bei jedem Push ausgeführt.

## 10. Abnahmekriterien:

- 100% Testabdeckung für `Wishlist` und `Category` Komponenten.
- Keine kritischen Fehler in den Unit-Tests für Utility-Funktionen und die `store` Klasse.

## 11. Testteam und Verantwortlichkeiten:

- Léonard Roch: Testautomatisierung, Testdurchführung, Auswertung und Testkonzept.
- Sananjayan Paramanantharajah: Erstellung des Testkonzepts, Unterstützung bei der Testdurchführung und Auswertung.
